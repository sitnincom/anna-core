module.exports = {
    "Application": require("./lib/AppModule"),
    "Model": require("./lib/Model"),
    "AppModule": require("./lib/AppModule"),
    "WebApplication": require("./lib/WebApplication"),
    "WebAppModule": require("./lib/WebAppModule"),
    "ConfDirReader": require("./lib/ConfDirReader"),
    "Theme": require("./lib/Theme"),
    "misc": require("./lib/misc"),
};
