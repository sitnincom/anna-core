"use strict";

const path = require("path");
const async = require("asyncawait").async;
const await = require("asyncawait").await;

module.exports = class Application {
    constructor (config) {
        this.__config = Object.assign(this.defaultConfig, config);
        this.__node_modules = new Map();
        this.__app_modules = new Map();
        this.__models = new Map();

        this.addNodeModule("log4js").configure(this.config.logger);
        this.addNodeModule("moment-timezone").locale(this.config.locale);
        this.__pg = this.addNodeModule("pg-promise")()(this.config.postgres);
    }

    get defaultConfig () {
        return {};
    }

    get config () {
        return this.__config;
    }

    get log () {
        return this.getNodeModule("log4js").getLogger(this.constructor.name);
    }

    get db () {
        return this.__pg;
    }

    get modules () {
        return this.__app_modules;
    }

    getModel (name) {
        const model = this.__models.get(name);

        if (!model) {
            throw new Error(`Error trying to get undefined model ${name}`);
        }

        return model;
    }

    addNodeModule (name) {
        const module_instance = require(name);
        this.__node_modules.set(name, module_instance);
        return module_instance;
    }

    getNodeModule (name) {
        return this.__node_modules.get(name);
    }

    __loadModules () {
        Object.keys(this.config.modules).forEach(module_name => {
            const module_defs = this.config.modules[module_name];

            let module_src = module_defs.path;

            if (module_src.substr(0,1) == ".") {
                module_src = path.resolve(path.join(process.cwd(), module_defs.path));
            }

            const module_class = require(module_src);
            this.__app_modules.set(module_name, new module_class(this, module_defs.config || {}));
        });

        return Promise.resolve();
    }

    __loadModels () {
        this.__app_modules.forEach((module, name) => {
            if (module.models) {
                module.models.forEach(model_class => {
                    if (this.__models.has(model_class.name)) {
                        throw new Error(`Model ${model_class.name} already registered`);
                    } else {
                        this.__models.set(model_class.name, new model_class(this));
                    }
                });
            }
        });

        return Promise.resolve();
    }

    __initAppModules () {
        return async(() => {
            this.modules.forEach((module, name) => {
                await(module.init());
            });
        })();
    }

    __preloadModelsData () {
        return async(() => {
            this.__models.forEach((model, name) => {
                if (model.shouldBePreloaded) {
                    this.log.info(`Preloading data for model ${name}`);
                    const data = await(model.loadAll(true));
                }
            });
        })();
    }

    init () {
        return new Promise((resolve, reject) => {
            this.__loadModules()
            .then(this.__loadModels.bind(this))
            .then(this.__initAppModules.bind(this))
            .then(this.__preloadModelsData.bind(this))
            .then(() => {
                resolve(this);
            }).catch(err => {
                reject(err);
            });
        });
    }
};
