"use strict";

const express = require("express");
const nunjucks = require("nunjucks");
const jumanji = require("jumanji");
const session = require('express-session');
const path = require("path");
const pgSessionStore = require('connect-pg-simple')(session);
const async = require("asyncawait").async;
const await = require("asyncawait").await;

const Application = require("./Application");
const WebAppModule = require("./WebAppModule");

module.exports = class WebApplication extends Application {
    constructor (config) {
        super(config);

        this.__express = null;
        this.__themes = new Map();
    }

    get themes () {
        return this.__themes;
    }

    get express () {
        return this.__express;
    }

    setupExpress () {
        this.__node_modules.set("express", express);
        this.__express = express();

        this.express.locals.config = this.config;

        this.express.set("trust proxy", true);
        this.express.set("case sensitive routing", true);
        this.express.set("strict routing", true);
        this.express.set("x-powered-by", false);

        // Обход ошибки кеширования в Safari
        this.express.use(jumanji);

        this.express.use(session({
            saveUninitialized: false,
            resave: true,
            rolling: true,
            prefix: `${this.config.auth.session.prefix}:`,
            secret: this.config.auth.session.secret,
            maxAge: this.config.auth.session.maxAge,
            store: new pgSessionStore({
                conString: this.config.postgres,
                schemaName: "auth",
                tableName: "sessions",
            }),
        }));
    }

    __updateLocals (req, res, next) {
        //this.log.debug("__updateLocals", req._parsedUrl.pathname);
        const cfg = this.config;

        res.locals.core = this;

        res.locals.ui = {
            current_url: req._parsedUrl.pathname,
            counters_enabled: cfg.site.counters_enabled,
        };

        if (req.user && req.user.id) {
            res.locals.ui.main_menu = cfg.main_menu.known;
        } else {
            res.locals.ui.main_menu = cfg.main_menu.guest;
        }


        res.locals.site = {
            title: cfg.site.title,
            domain: cfg.site.domain,
        };

        if (req.user) {
            res.locals.user = req.user;
        }

        this.modules.forEach(module => {
            if (module instanceof WebAppModule) {
                module.updateResLocals.bind(module)(req, res);
            }
        });

        next();
    }

    setupModuleMiddlewares () {
        this.modules.forEach((module, name) => {
            if (module.globalMiddlewares) {
                module.globalMiddlewares.forEach(mw => {
                    this.express.use(mw);
                });
            }
        });

        this.express.use(this.__updateLocals.bind(this));
    }

    setupRouters () {
        this.modules.forEach((module, name) => {
            if (module.router) {
                this.log.info(`Mounting ${name} router`);
                this.express.use(module.router);
            } else {
                this.log.debug(`Skipping ${name} due to lack of router`);
            }
        });
    }

    setupFinalizers () {
        this.modules.forEach((module, name) => {
            if (module.finalizers) {
                this.log.info(`Mounting ${name} finalizers`);
                module.finalizers.forEach(finalizer => {
                    this.express.use(finalizer);
                });
            } else {
                this.log.debug(`Skipping ${name} due to lack of router`);
            }
        });
    }

    init () {
        return super.init()
        .then(this.setupExpress.bind(this))
        .then(this.setupModuleMiddlewares.bind(this))
        .then(this.setupRouters.bind(this))
        //.then(this.setupFinalizers.bind(this))
        ;
    }

    run () {
        const port = this.config.site.bind.port;
        const host = this.config.site.bind.host;

        this.express.listen(port, host, () => {
            this.log.info(`Listening at http://${host}:${port}`);
        });
    }
};
