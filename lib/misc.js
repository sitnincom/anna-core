"use strict";

const _ = require("lodash");

// Возвращает true, если в массиве values присутствуют хотя бы одно значени из массива array
exports.hasOneOf = function (array, values) {
    return _.intersection(array, values).length > 0;
};

// Возвращает true, если в массиве values присутствуют все значения из массива array
exports.hasAllOf = function (array, values) {
    const res = _.intersection(array, values);
    //console.log(array, values, array.length, res.length, res.length == array.length);
    return res.length == array.length;
};

// Возвращается true, если hasAllOf выполняется хотя бы для одного массива rules
// Возвращает true, если rules == null
exports.anyRule = function (rules, tags) {
    return rules ? rules.filter(rule => {
            //console.log(rule, tags, exports.hasAllOf(rule, tags));
            return exports.hasAllOf(rule, tags);
        }).length > 0 : true;
};

exports.anyOne = function (rules, tags) {
    return rules ? rules.filter(rule => {
            //console.log(rule, tags, exports.hasOneOf(rule, tags));
            return exports.hasOneOf(rule, tags);
        }).length > 0 : true;
};

exports.getRandomInt = function (min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
};
