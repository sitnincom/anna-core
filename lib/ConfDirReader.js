"use strict";

const path = require("path");
const fs = require("fs");
const lodash = require("lodash");
const json5 = require("json5");
const Promise = require("bluebird");

module.exports = class ConfDirReader {
    loadJsonFile (filename) {
        return new Promise((resolve, reject) => {
            fs.readFile(filename, { encoding: "utf-8" }, (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    let res = undefined;
                    try {
                        res  = json5.parse(data);
                        resolve(res);
                    } catch (err) {
                        console.error(`Error parsing config file ${filename}:\n${err.message}`);
                        reject(err);
                    }
                }
            });
        });
    }

    load (dirname) {
        return new Promise((resolve, reject) => {
            const _real_dirname = path.resolve(dirname);
            fs.readdir(_real_dirname, (err, files) => {
                if (err) {
                    reject(err);
                } else {
                    const loaders = [];

                    files.sort().forEach(filename => {
                        const _real_filename = path.join(_real_dirname, filename);
                        loaders.push(this.loadJsonFile(_real_filename));
                    });

                    Promise.all(loaders).then(contents => {
                        let res = {};
                        contents.forEach(obj => {
                            lodash.merge(res, obj);
                        });
                        resolve(res);
                    }).catch(err => {
                        console.error(`Error while parsing configuration`);
                        reject(err);
                    });
                }
            });
        });
    }
};
