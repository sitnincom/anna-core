"use strict";

module.exports = class AppModule {
    constructor (app, config) {
        this.__app = app;
        this.__config = Object.assign(this.defaultConfig, config);
    }

    get defaultConfig () {
        return {};
    }

    get config () {
        return this.__config;
    }

    get app () {
        return this.__app;
    }

    get log () {
        return this.app.getNodeModule("log4js").getLogger(this.constructor.name);
    }

    get db () {
        return this.app.db;
    }

    get models () {
        return null;
    }

    init () {
        this.log.warn(`Empty ${this.constructor.name} init()`);
        return Promise.resolve();
    }
};
