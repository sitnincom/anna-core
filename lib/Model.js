"use strict";

const path = require("path");
const async = require("asyncawait").async;
const await = require("asyncawait").await;

module.exports = class Model {
    constructor (app, config) {
        this.__app = app;

        this.__config = Object.assign({
            minify: false,
            debug: true,
        }, config);

        this.shouldBePreloaded = false;

        this.__cache = new Map();
        this.__sqls = new Map();
    }

    get app () {
        return this.__app;
    }

    get db () {
        return this.app.db;
    }

    get log () {
        return this.app.getNodeModule("log4js").getLogger(`${this.constructor.name} Model`);
    }

    get loadByIdQueryFile () {
        throw new Error("loadByIdQueryFile property SHOULD be overriden");
    }

    get loadAllQueryFile () {
        throw new Error("loadAllQueryFile property SHOULD be overriden");
    }

    get __dirname () {
        throw new Error("__dirname property SHOULD be overriden");
    }

    sqlFile (filename) {
        const __real_fn = path.resolve(path.join(this.__dirname, `sql/${filename}.sql`));
        let result = this.__sqls.get(__real_fn);

        if (!result) {
            result = this.app.getNodeModule("pg-promise").QueryFile(__real_fn, this.__config);
            this.__sqls.set(__real_fn, result);
        }

        return result;
    }

    cacheObjects (data) {
        Object.keys(data).forEach(key => {
            this.__cache.set(key, data[key]);
        });
    }

    listAll () {
        const res = {};

        this.__cache.forEach((key, value) => {
            res[key] = valuse;
        });

        return res;
    }

    listAllAsArray (sorter) {
        const res = Array.from(this.__cache.values());
        if (sorter) { res.sort(sorter); }
        return res;
    }

    loadAll (cacheEnabled) {
        this.log.debug(`${this.constructor.name} loadAll() called`);

        return async(() => {
            let res = null;

            const query = this.sqlFile(this.loadAllQueryFile);
            const rows = await(this.db.any(query));

            if (rows) {
                res = [];
                rows.forEach(row => {
                    const obj = Object.assign({}, row);
                    res.push(obj);
                    if (cacheEnabled) {
                        this.__cache.set(obj.id, obj);
                    }
                });
            }

            this.log.debug(`${this.constructor.name} loadAll() loaded ${res.length} items`);
            return res;
        })();
    }

    loadFromDb (id) {
        return async(() => {
            let obj = null;

            const query = this.sqlFile(this.loadByIdQueryFile);
            const db_obj = await(this.db.oneOrNone(query, id));

            if (db_obj) {
                obj = Object.assign({}, db_obj);
            }

            return obj;
        })();
    }

    getById (id) {
        return async(() => {
            let result = this.__cache.get(id);

            if (!result) {
                result = await(this.loadFromDb(id));
                this.__cache.set(id, result);
            }

            return result;
        })();
    }
};
