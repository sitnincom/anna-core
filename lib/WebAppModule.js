"use strict";

const Router = require("express").Router;
const AppModule = require("./AppModule");

module.exports = class WebAppModule extends AppModule {
    constructor (app, config) {
        super(app, config);

        this.__router = null;
    }

    get express () {
        return this.app.express;
    }

    get globalMiddlewares () {
        return null;
    }

    get router () {
        if (!this.__router) {
            this.__router = new Router({
                strict: true,
                caseSensitive: true,
            });
        }

        return this.__router;
    }

    get finalizers () {
        return [];
    }

    get defaultTheme () {
        return this.app.themes.get("default");
    }

    updateResLocals (req, res) {}
};
