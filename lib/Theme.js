"use strict";

const path = require("path");
const WebAppModule = require("./WebAppModule");

module.exports = class Theme extends WebAppModule {
    get defaultConfig () {
        return {
            templates: path.resolve(path.join(this.__dirname, "templates")),
            url: `/themes/${this.name}/`,
        };
    }

    get __dirname () {
        throw new Error("Theme ancestor's __dirname property SHOULD be overrided!");
    }

    get name () {
        return "default";
    }
    get templates () {
        return this.config.templates;
    }

    get url () {
        return this.config.url;
    }

    init () {
        return super.init().then(() => {
            this.app.themes.set(this.name, this);
        });
    }

    render (template, res, data) {
        res.locals.ui.theme = this.config.url;
        const eff_data = Object.assign({}, res.locals, data || {});
        this.__env.render(template, eff_data, (err, content) => {
            if (err) {
                throw new Error(err);
            } else {
                res.send(content);
            }
        });
    }
};
